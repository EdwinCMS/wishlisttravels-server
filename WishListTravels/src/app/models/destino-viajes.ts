import {v4 as uuid} from 'uuid';

export class DestinoViajes {
  selected: boolean;    
    servicios: string[];
    id = uuid();
  
  constructor( public nombre:string, public url:string, public votes: number = 0){      
    this.servicios = ['Pileta','Desayuno'];  
}

  getIsSelected(): boolean{
    return this.selected;//lo envia a la vista destino-viajes
  }
  setSelected(s:boolean){
    this.selected = s;//recibe del componente lista-destinos
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}
