import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, APP_INITIALIZER, Injectable } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule  }from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap, StoreModule, Store }from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import  Dexie  from 'dexie';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { Observable, from } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajesComponent } from './components/destino-viajes/destino-viajes.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetallesComponent } from './components/destino-detalles/destino-detalles.component';
import { FormDestinoViajesComponent } from './components/form-destino-viajes/form-destino-viajes.component';
import { DestinosApiClient } from './models/destinos-api-client';
import { DestinosViajesState, reducerDestinosViajes, intializeDestinosViajesState,DestinosViajesEffects, InitMyDataAction} from './models/destino-viajes-state';
import { DestinoViajes } from './models/destino-viajes';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component';

import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentsComponent } from './components/vuelos/vuelos-components/vuelos-components.component';
import { VuelosMainComponentsComponent } from './components/vuelos/vuelos-main-components/vuelos-main-components.component';
import { VuelosMasInfoComponentsComponent } from './components/vuelos/vuelos-mas-info-components/vuelos-mas-info-components.component';
import { VuelosDetalleComponentsComponent } from './components/vuelos/vuelos-detalle-components/vuelos-detalle-components.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';


// app config
export interface AppConfig {
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin app config

//Rutas

// init routing
export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponentsComponent },
  { path: 'mas-info', component: VuelosMasInfoComponentsComponent },
  { path: ':id', component: VuelosDetalleComponentsComponent },
];
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetallesComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard ]
  },
  {
    path: 'vuelos',
    component: VuelosComponentsComponent,
    canActivate: [ UsuarioLogueadoGuard ],
    children: childrenRoutesVuelos
  }
];
//fin ini routing

// app init
export function init_app(appLoadService: AppLoadService): () => Promise<any>  {
  return () => appLoadService.intializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async intializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// fin app init

// redux init
  export interface AppState {
    destinos: DestinosViajesState;
  }
  const reducers: ActionReducerMap<AppState> = {
    destinos: reducerDestinosViajes
  };

  const reducersInitialState = {
    destinos: intializeDestinosViajesState()
  };
// fin redux init

// dexie db
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) {}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie {
  destinos: Dexie.Table<DestinoViajes, number>;  
  translations: Dexie.Table<Translation, number>;
  constructor () {
      super('MyDatabase');
      this.version(1).stores({
        destinos: '++id, nombre, imagenUrl'
      });
      this.version(2).stores({
        destinos: '++id, nombre, imagenUrl',
        translations: '++id, lang, key, value'
      });
    }
  }
  
  export const db = new MyDatabase();
  // fin dexie db

  // i18n ini
class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                                        if (results.length === 0) {
                                          return this.http
                                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang)
                                            .toPromise()
                                            .then(apiResults => {
                                              db.translations.bulkAdd(apiResults);
                                              return apiResults;
                                            });
                                        }
                                        return results;
                                      }).then((traducciones) => {
                                        console.log('traducciones cargadas:');
                                        console.log(traducciones);
                                        return traducciones;
                                      }).then((traducciones) => {
                                        return traducciones.map((t) => ({ [t.key]: t.value}));
                                      });
    /*
    return from(promise).pipe(
      map((traducciones) => traducciones.map((t) => { [t.key]: t.value}))
    );
    */
   return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}
//find init i18n

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajesComponent,
    ListaDestinosComponent,
    DestinoDetallesComponent,
    FormDestinoViajesComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentsComponent,
    VuelosMainComponentsComponent,
    VuelosMasInfoComponentsComponent,
    VuelosDetalleComponentsComponent,

    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    //Formularios
    FormsModule,
    ReactiveFormsModule,
    //Rutas
    //registro de las rutas, vinculacion al decorador @NgModule
    AppRoutingModule,
    RouterModule.forRoot(routes),    
    NgRxStoreModule.forRoot(reducers, { 
      initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      } }),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (HttpLoaderFactory),
          deps: [HttpClient]
      }
    }),
  ],
  providers: [
    DestinosApiClient,
    AuthService,
    UsuarioLogueadoGuard,
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true }    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
