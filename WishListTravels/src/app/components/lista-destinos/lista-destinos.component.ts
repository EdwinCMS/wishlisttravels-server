import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DestinoViajesComponent } from '../destino-viajes/destino-viajes.component';
import { DestinoViajes } from '../../models/destino-viajes';
import { DestinosApiClient } from '../../models/destinos-api-client';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destino-viajes-state';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViajes>;  
  updates: string[];
  all; 

  constructor(public destinosApiClient:DestinosApiClient, private store: Store<AppState>) {
    //this.destinos = [];//es igual a un arreglo vacio
    this.onItemAdded = new EventEmitter();
    this.updates=[];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if(d != null){
          this.updates.push('se ha elegido a ' + d.nombre)
        }
      });
      store.select(state => state.destinos.items).subscribe(items => this.all = items);
   }
  
  ngOnInit(): void {
  }
  agregado(d: DestinoViajes){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  /*
  guardar(nombre:string, url:string):boolean{//boolean: uso comun para una "single page aplication" una sola pagina
    let nuevoElemento = new DestinoViajes(nombre, url);//instancia y recibe los valores del formulario
    this.destinos.push(nuevoElemento); //agrega nuevo elemento al arreglo
    return false; //para que no recargue la pagina, por defecto es 'true'
  }*/
  elegido(e: DestinoViajes){
    //marca seleccionado y deselecciona los demas
    this.destinosApiClient.elegir(e);//coge mi array de destinos y deselecciona todos
    //e.setSelected(true);//marca al elegido seleccionado
  }
  getAll(){

  }
}
