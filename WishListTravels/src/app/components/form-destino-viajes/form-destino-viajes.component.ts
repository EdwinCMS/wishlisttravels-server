import { Component, OnInit, EventEmitter, Output, Inject, forwardRef } from '@angular/core';
import { DestinoViajes } from '../../models/destino-viajes';
import { FormGroup, FormBuilder, Validators, FormControl, ControlContainer, ValidatorFn } from '@angular/forms'
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viajes',
  templateUrl: './form-destino-viajes.component.html',
  styleUrls: ['./form-destino-viajes.component.css']
})
export class FormDestinoViajesComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViajes>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      //Aqui se llaman los validadores
      //'compose': coleccion de validaciones
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametizable(this.minLongitud),
      ])],
      url: ['']
    })
   }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
  fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(120),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }
  guardar(nombre: string, url: string): boolean{
    const d = new DestinoViajes(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }
  
  nombreValidator(control: FormControl):{ [s: string]:boolean }{
    const l = control.value.toString().trim().length;
    if(l>0 && l<5){
      return { invalidNombre: true };
    }
    return null;
  }
 nombreValidatorParametizable(minLong: number): ValidatorFn{
  return(control: FormControl): {[s:string]:boolean}| null => {const l = control.value.toString().trim().length;
    if(l>0 && l<minLong){
      return { minLongNombre: true };
    }
  return null;
  }
 } 
}
