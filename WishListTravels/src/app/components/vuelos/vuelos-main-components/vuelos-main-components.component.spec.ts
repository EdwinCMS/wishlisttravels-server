import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuelosMainComponentsComponent } from './vuelos-main-components.component';

describe('VuelosMainComponentsComponent', () => {
  let component: VuelosMainComponentsComponent;
  let fixture: ComponentFixture<VuelosMainComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuelosMainComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuelosMainComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
