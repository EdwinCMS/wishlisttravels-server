import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuelosMasInfoComponentsComponent } from './vuelos-mas-info-components.component';

describe('VuelosMasInfoComponentsComponent', () => {
  let component: VuelosMasInfoComponentsComponent;
  let fixture: ComponentFixture<VuelosMasInfoComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuelosMasInfoComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuelosMasInfoComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
